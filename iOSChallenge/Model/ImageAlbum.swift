//
//  ImageAlbum.swift
//  iOSChallenge
//
//  Created by Daniel Perez on 15-01-19.
//  Copyright © 2019 Daniel Perez. All rights reserved.
//

import Foundation

struct ImageAlbum {
    let title: String?
    let images: String?
    
    init (dictionary: [String:AnyObject]){
        self.title = dictionary["title"] as? String
        self.images = dictionary["link"] as? String
    }
}
