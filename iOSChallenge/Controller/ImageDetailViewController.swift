//
//  ImageDetailViewController.swift
//  iOSChallenge
//
//  Created by Daniel Perez on 16-01-19.
//  Copyright © 2019 Daniel Perez. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ImageDetailViewController: UIViewController {
    
    @IBOutlet weak var imageDetailView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = ApiClient.ImageTitle
        imageDetailView.sd_setImage(with: URL(string: ApiClient.ImageUrl), placeholderImage: UIImage(named: "placeholder.png"))
    }
}
