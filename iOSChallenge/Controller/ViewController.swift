//
//  ViewController.swift
//  iOSChallenge
//
//  Created by Daniel Perez on 15-01-19.
//  Copyright © 2019 Daniel Perez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imageTxt: UITextField!
    @IBOutlet weak var saerchBtn: UIButton!
    
    var ImagesArray = [ImageAlbum]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func OnButtonPressed(_ sender: UIButton) {
        if (self.imageTxt.text != "") {
            self.Call()
        } else {
            self.imageTxt.becomeFirstResponder()
        }
    }
    
    func Call()
    {
        ApiClient.CallImgurApi(searchVariable: self.imageTxt.text!){(images) in
            if let im = images {
                self.ImagesArray = im
                print("nro de array:",self.ImagesArray.count)
            }
            self.collectionView.reloadSections(IndexSet(integer: 0))
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("collection view",ImagesArray.count)
        return ImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! ImageCollectionViewCell
        let images = ImagesArray[(indexPath as NSIndexPath).row]
        let imagetitle = images.title
        let imageUrl = images.images

        cell.displayImages(image: imageUrl!, title: imagetitle ?? "Without title")
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageData = ImagesArray[(indexPath as NSIndexPath).row]
        ApiClient.ImageTitle = imageData.title ?? "Without Title"
        ApiClient.ImageUrl = imageData.images ?? ""
        performSegue(withIdentifier: "toImageDetailSegue", sender: nil)
    }
    


}

