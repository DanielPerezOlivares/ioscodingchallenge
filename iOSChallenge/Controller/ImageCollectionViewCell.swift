//
//  ImageCollectionViewCell.swift
//  iOSChallenge
//
//  Created by Daniel Perez on 15-01-19.
//  Copyright © 2019 Daniel Perez. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgurImage: UIImageView!
    @IBOutlet weak var imgurTitle: UILabel!

    func displayImages (image: String, title: String){
        imgurTitle.text = title
        imgurImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "placeholder.png"))

    }
}
