//
//  ApiClient.swift
//  iOSChallenge
//
//  Created by Daniel Perez on 15-01-19.
//  Copyright © 2019 Daniel Perez. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct ApiClient {
    
    static var ImageTitle = ""
    static var ImageUrl = ""
    
    static func CallImgurApi(searchVariable: String, completion: @escaping (_ image: [ImageAlbum]?) -> ()) {
        let headers: HTTPHeaders = [
            "Authorization":"Client-ID 126701cd8332f32",
            "Accept": "application/json"
        ]
        print("varaible a buscar: ", searchVariable)
        let url: String? = "https://api.imgur.com/3/gallery/search/time/1?q="+searchVariable
        
        Alamofire.request(url!, headers: headers)
            .responseJSON { response in
                
                let tmp: NSMutableArray = []
                
                switch response.result {
                case .failure( _):
                    if let data = response.data {
                        print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                    }
                    completion(nil)
                    
                case .success(let value):
                    let json = JSON(value)
                    print("Imagenes", json)
                    for current in JSON(json)["data"].arrayValue {
                        if (current["type"] == "image/jpeg" || current["type"] == "image/png"){
                            tmp.add(ImageAlbum(dictionary: (current.dictionaryObject! as NSDictionary) as! [String : AnyObject]))
                        }
                        for item in current["images"].arrayValue {
                            if (item["type"] == "image/jpeg" || item["type"] == "image/png"){
                                //print("link de imagenes:", item["link"])
                                //print("titulo imagenes:", item["description"])
                                
                                tmp.add(ImageAlbum(dictionary: (item.dictionaryObject! as NSDictionary) as! [String : AnyObject]))
                            }
                            
                        }
                    }
                    completion(tmp as NSArray as? [ImageAlbum])
                    
                }
        }
    }
}
